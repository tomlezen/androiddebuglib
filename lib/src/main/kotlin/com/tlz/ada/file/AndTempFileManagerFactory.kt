package com.tlz.ada.file

import android.content.Context
import com.tlz.ada.executeSafely
import fi.iki.elonen.NanoHTTPD
import java.io.File

/**
 * Created by Tomlezen.
 * Data: 2018/9/5.
 * Time: 12:51.
 */
class AndTempFileManagerFactory(private val ctx: Context) : NanoHTTPD.TempFileManagerFactory {

    override fun create(): NanoHTTPD.TempFileManager =
        AndTempFileManager(ctx.externalCacheDir.absolutePath + "/AndDevelopAssistant")

    class AndTempFileManager(tmpdir: String) : NanoHTTPD.TempFileManager {

        private val tempDirFile = File(tmpdir)
        private val tempFiles = mutableListOf<NanoHTTPD.TempFile>()

        init {
            if (!tempDirFile.exists()) {
                tempDirFile.mkdirs()
            } else {
                // 清空之前未删除掉的文件
                executeSafely { tempDirFile.listFiles().forEach { it.delete() } }
            }
        }

        override fun clear() {
            this.tempFiles.forEach {
                executeSafely { it.delete() }
            }
            this.tempFiles.clear()
        }

        override fun createTempFile(filename_hint: String?): NanoHTTPD.TempFile {
            val tempFile = NanoHTTPD.DefaultTempFile(tempDirFile)
            this.tempFiles.add(tempFile)
            return tempFile
        }
    }

}